package com.carpool.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

@Component
public class RequestTransporter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTransporter.class);
	
	private AtomicInteger requestIdGenerator = new AtomicInteger(0);
	
	private boolean isShutdownTriggered = false;
	private boolean isDownlinkActive = false;
	
	private JSONObject lastResponseFromDownlink = null;
	private DeferredResult<String> downlinkResult = null;
	
	private Map<Integer,DeferredResult<String>> pendingResultMap = new HashMap<Integer,DeferredResult<String>>();
	private Queue<JSONObject> pendingRequestQueue = new LinkedBlockingQueue<JSONObject>(100);
	
	private Thread requestProcessor;
	
	private class RequestProcessor implements Runnable {
		public void run() {

			while(!isShutdownTriggered) {
				
				
				if(isDownlinkActive) {					
					// First process any received response
					processResponseFromDownlink();
					
					routeWebhookRequestToDownlink();
					
					// Wait for 10 ms before checking response
					try {
						TimeUnit.MILLISECONDS.sleep(10);
					} catch (InterruptedException e) {
						// do nothing
					}
				} else {
					sendErrorResponseToQueuedRequests();
					
					try {
						TimeUnit.MILLISECONDS.sleep(100);
					} catch (InterruptedException e) {
						// do nothing
					}
				}
			}
			
		}
	}
	
	@PostConstruct
	public void init() {
		requestProcessor = new Thread(new RequestProcessor());
		requestProcessor.start();
	}
	
	@PreDestroy
	public void destroy() throws InterruptedException {
		isShutdownTriggered = true;
		requestProcessor.join();
	}
	
	private synchronized void processResponseFromDownlink() {
		
		if(lastResponseFromDownlink != null) {
			Integer requestId = new Integer(lastResponseFromDownlink.getInt("id"));
			
			// Request Id 0 also means no previous submitted request
			if(requestId == 0) {
				lastResponseFromDownlink = null;
				return;
			}
			
			if(pendingResultMap.containsKey(requestId)) {
				DeferredResult<String> resultToProcess = pendingResultMap.get(requestId);
				pendingResultMap.remove(requestId);
				String webhookResponse = lastResponseFromDownlink.getString("response");
							
				if(webhookResponse == null) {
					LOGGER.warn("No Response element present in JSON from downlink for Id # {}", requestId);
					resultToProcess.setErrorResult("404 Not found");

				} else {
					resultToProcess.setResult(webhookResponse);
				}
				
			} else {
				LOGGER.warn("No pending Result record found for received response # {}. Something is wrong", requestId);
			}
			
			lastResponseFromDownlink = null;
		}
	}
	
	private synchronized void routeWebhookRequestToDownlink() {
		JSONObject nextWebhookRequest = pendingRequestQueue.poll();
		
		if(nextWebhookRequest != null) {
			LOGGER.debug("Sending downlink Response as {}", nextWebhookRequest.toString());
			downlinkResult.setResult(nextWebhookRequest.toString());
		}
	}
	
	private synchronized void sendErrorResponseToQueuedRequests() {
		while(!pendingRequestQueue.isEmpty()) {
			JSONObject webhookRequest = pendingRequestQueue.poll();
			Integer requestId = webhookRequest.getInt("id");
			
			DeferredResult<String> resultToProcess = pendingResultMap.get(requestId);
			pendingResultMap.remove(requestId);
			
			LOGGER.info("Sending 404 Error Response for Request # {} as downlink is not yet Active", requestId);
			resultToProcess.setErrorResult("404 Not Found");
		}
	}
	
	private synchronized void queueRequest(int requestId, JSONObject request, DeferredResult<String> result) {
		JSONObject requestWrapper = new JSONObject();
		requestWrapper.put("id", requestId);
		requestWrapper.put("request", request);
		
		pendingResultMap.put(requestId, result);
		pendingRequestQueue.add(requestWrapper);
	}
	
	public DeferredResult<String> submitRequestForDownlink(JSONObject request) {
		
		int requestId = requestIdGenerator.incrementAndGet();
		LOGGER.info("Submitting Request # {} for downlink routing", requestId);
		
		DeferredResult<String> result = new DeferredResult<String>(4000L, new TimeoutException());
		
		queueRequest(requestId, request, result);
		return result;
	}
	
	public DeferredResult<String> handleResponseFromDownlink(JSONObject response) {
		
		if(!isDownlinkActive) {
			// Activate downlink on first request
			isDownlinkActive = true;
		} else {
			LOGGER.info("Received response # {} from downlink", response.get("id"));
		}
		
		if(lastResponseFromDownlink != null) {
			LOGGER.warn("Last Response not yet processed. Something is wrong");
			return null;
		}
		
		lastResponseFromDownlink = response;
		downlinkResult = new DeferredResult<String>();
		
		return downlinkResult;
	}
}
