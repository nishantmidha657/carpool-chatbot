package com.carpool.util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class WebhookRequestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(WebhookRequestController.class);
	
	@Autowired
	private RequestTransporter requestTransporter;
	
	@RequestMapping(value = "/webhook", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DeferredResult<String> handleRequest(@RequestBody String webhookRequest) {
		LOGGER.debug("Received Webhook Request as {}", webhookRequest);
		JSONObject jsonRequest = new JSONObject(webhookRequest);
		return requestTransporter.submitRequestForDownlink(jsonRequest);
	}
	
	@RequestMapping(value = "/downlink", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DeferredResult<String> handleDownlinkResponse(@RequestBody String webhookResponseFromDownlink) {
		LOGGER.debug("Received Downlink Response as {}", webhookResponseFromDownlink);
		JSONObject jsonResponse = new JSONObject(webhookResponseFromDownlink);
		return requestTransporter.handleResponseFromDownlink(jsonResponse);
	}
	
	@ExceptionHandler(TimeoutException.class)
	public void handleTimeoutException(TimeoutException tex, HttpServletResponse response) {
		try {
			LOGGER.info("Timeout occured; Sending 404 Response");
			LOGGER.debug(ExceptionUtils.getFullStackTrace(tex));
			response.sendError(404, "Server Not Available");
		} catch (IOException e) {
			// Do Nothing
		}
	}
	
	@ExceptionHandler(Exception.class)
	public void handleException(Exception ex, HttpServletResponse response) {
		try {
			LOGGER.info("Exception occured; Sending 500 Response");
			LOGGER.debug(ExceptionUtils.getFullStackTrace(ex));
			response.sendError(404, "Internal Error");
		} catch (IOException e) {
			// Do Nothing
		}
	}
}
