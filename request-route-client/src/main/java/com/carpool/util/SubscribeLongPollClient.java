package com.carpool.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class SubscribeLongPollClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscribeLongPollClient.class);

	@Value("${requestRouter.url}")
	public String requestRouterUrl;

	@Value("${chatbot.url}")
	public String chatbotUrl;

	private boolean isShutdownInitiated = false;
	
	public String chatbotResponse = null;
	public int receivedRequestId = 0;
	
	private Thread requestForwarder;

	@PostConstruct
	void initialize() throws IOException {
		requestForwarder = new Thread(new WebhookRequestForwarder());
		requestForwarder.start();
	}
	
	@PreDestroy
	void deinitialize() throws InterruptedException {
		isShutdownInitiated = true;
		requestForwarder.join();
	}
	
	
	private class WebhookRequestForwarder implements Runnable {
		
		public void run() {
	
			while (!isShutdownInitiated) {
	
				LOGGER.info("Sending Poll Request to Router with chatbot response {}",
						chatbotResponse);
	
				HttpURLConnection serverConnection = sendHttpRequest("POST",
						constructNextRequest(chatbotResponse), requestRouterUrl);
				
				// Wait and retry if Router downlink URL is unavailable
				if(serverConnection == null) {
					waitForOneSecond();
					continue;
				}
				
				chatbotResponse = null;
				receivedRequestId = 0;
	
				// Recieved query for chatbot backend
				String botRequest;
				try {
					botRequest = IOUtils.toString(serverConnection.getInputStream());
				} catch (IOException ioex) {
					LOGGER.info("Exception while reading Server response : {}", ioex.toString());
					continue;
				}
				
				String webhookRequest = extractRequestPart(botRequest);
				
				LOGGER.info("Received request from Router : {}", webhookRequest);
				serverConnection.disconnect();
				
				// No need to invoke webhook application, if request is null
				if(webhookRequest == null) {
					waitForOneSecond();
					continue;
				}
				
				// Send query to chatbot backend
				HttpURLConnection chatbotAppConnection = sendHttpRequest("POST",
						webhookRequest, chatbotUrl);
				
				// Set the bot response as next Request
				try {
					chatbotResponse = IOUtils.toString(chatbotAppConnection
							.getInputStream());
				} catch (IOException ioex) {
					LOGGER.info("Exception while reading Chatbot response : {}", ioex.toString());
				}
				
				LOGGER.info("Received Chatbot response as {}", chatbotResponse);
	
				chatbotAppConnection.disconnect();	
			}
	
		}

	}

	private HttpURLConnection sendHttpRequest(String typeOfRequest,
			String body, String serverUrl) {

		HttpURLConnection httpURLConnection = null;
		try {
			URL url = new URL(serverUrl);
			URLConnection con = url.openConnection();
	
			httpURLConnection = (HttpURLConnection) con;
			httpURLConnection.setRequestMethod(typeOfRequest);
			httpURLConnection.setRequestProperty("Content-Type",
					"application/json");
			if (body != null) {
				
				httpURLConnection.setDoOutput(true);
				httpURLConnection.getOutputStream().write(body.getBytes());
			}
			httpURLConnection.connect();
		} catch(Exception ex) {
			LOGGER.error("Exception opening Http connection to {} : {}",
							serverUrl, ex.toString());
			return null;
		}

		return httpURLConnection;
	}
	
	private String extractRequestPart(String routerResponse) {
		try {
			LOGGER.debug("Parsing received response from Server into JSON : {}", routerResponse);
			JSONObject requestWrapper = new JSONObject(routerResponse);
			
			receivedRequestId = requestWrapper.getInt("id");
			if(receivedRequestId == 0) {
				LOGGER.warn("Response received from Server is invalid; no request id found");
				return null;
			}
			
			String requestPart = requestWrapper.get("request").toString();
			
			LOGGER.debug("Extracted webhook request with id {} from Server response", receivedRequestId);
			return requestPart;
		} catch(Exception ex) {
			LOGGER.warn("Exception while parsing received Server response into Json : {}", ex.toString());
			return null;
		}
	}
	
	private String constructNextRequest(String webhookResponse) {
		JSONObject nextRequest = new JSONObject();
		nextRequest.put("id", receivedRequestId);
		nextRequest.put("response", webhookResponse);
		return nextRequest.toString();
	}

	private void waitForOneSecond() {
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			// do nothing
		}
	}
}
