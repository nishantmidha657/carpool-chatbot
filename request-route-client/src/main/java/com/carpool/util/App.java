package com.carpool.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class App {

	@Bean
	public SubscribeLongPollClient pollClient() {
		return new SubscribeLongPollClient();
	}
	
	public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
