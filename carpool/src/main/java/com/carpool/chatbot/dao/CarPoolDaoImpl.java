package com.carpool.chatbot.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carpool.chatbot.BookCarPool;
import com.carpool.chatbot.QueryCarPool;
import com.carpool.chatbot.entity.CarPoolBooking;
import com.carpool.chatbot.entity.CarPoolBookingId;
import com.carpool.chatbot.entity.CarPoolSlot;
import com.carpool.chatbot.entity.Destination;
import com.carpool.chatbot.entity.UserDetail;

@Transactional
public class CarPoolDaoImpl {

	public static final Logger  logger=LoggerFactory.getLogger(CarPoolDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");

	public UserDetail getUserDetail(String userId) {
		return sessionFactory.getCurrentSession().get(UserDetail.class, userId);
	}

	public List<UserDetail> getUserDetailByFirstName(String firstName) {
		return sessionFactory.getCurrentSession()
				.createCriteria(UserDetail.class)
				.add(Restrictions.eq("firstName", firstName)).list();
	}
	
	public void saveCarPoolOffer(CarPoolSlot carPoolSlot) {
		carPoolSlot.getDestination().setDestinationName(carPoolSlot.getDestination().getDestinationName().toLowerCase());
		sessionFactory.getCurrentSession().merge(carPoolSlot);
	}
	
	public CarPoolSlot getCarPoolSlot(int slotId) {
		return sessionFactory.getCurrentSession().get(CarPoolSlot.class, slotId);
	}

	public String checkCarPoolAlreadyAvailable(int destinationId, String userId) {

		Criteria criteria = sessionFactory
				.getCurrentSession()
				.createCriteria(CarPoolSlot.class)
				.add(Restrictions.and(Restrictions.eq(
						"destination.destinationId", destinationId),
						Restrictions.eq("userDetail.userId", userId)));
		
		List<CarPoolSlot> results = criteria.list();
		
		// Remove carpool entry (slot and bookings) if for past day
		if (results.size() !=0 && results.get(0).getOfferTime().getDate() < new Date().getDate()){
			int recordsFound = criteria.list().size();
			Session session = sessionFactory.getCurrentSession();
			String selectBookedSlotId = "select CPS.slotId from CarPoolSlot CPS where CPS.userDetail.userId = :userId and CPS.destination.destinationId = :destinationId";
			Query querySlotId = session.createQuery(selectBookedSlotId).setParameter("userId", userId).setParameter("destinationId", destinationId);
			int slotId = (int) querySlotId.getResultList().get(0);
			
			String deleteCarPoolBookingSlot = "delete from CarPoolBooking CPB where CPB.carPoolSlot.slotId = :slotId";
			Query queryDeleteCarPoolBookingSlot = session.createQuery(deleteCarPoolBookingSlot).setParameter("slotId", slotId);
			
			String deleteCarPoolSlot = "delete from CarPoolSlot C where C.userDetail.userId = :userId and C.destination.destinationId = :destinationId";
			Query queryDeleteCarPoolSlot = session.createQuery(deleteCarPoolSlot).setParameter("userId", userId).setParameter("destinationId", destinationId);
			
			queryDeleteCarPoolBookingSlot.executeUpdate();
			queryDeleteCarPoolSlot.executeUpdate();
			
			return null;
		}
		else if(results.size() != 0) {
			
			// Return time in HH:MM (AM|PM) format of saved carpool
			return dateFormat.format(results.get(0).getOfferTime());
		}

		return null;
	}
	
	public List<Destination> findDestination(String destination) {

		Query query = sessionFactory.getCurrentSession().getNamedQuery(
				Destination.FIND_DESTINATION);

		query.setParameter("destinationName", destination.toLowerCase());
		
		return query.getResultList();
	}

	public List<CarPoolSlot> getCarPoolSlot(BookCarPool queryCarpoolSlot,
			int destinationId) {

		Criteria criterion = sessionFactory
				.getCurrentSession()
				.createCriteria(CarPoolSlot.class)
				.add(Restrictions.and(
						Restrictions.eq("destination.destinationId", destinationId),
						Restrictions.ne("userDetail.userId", queryCarpoolSlot.getUserId())));

		if (queryCarpoolSlot.getNoOfSlots() != 0) {
			criterion.add(Restrictions.ge("freeSlot",
					queryCarpoolSlot.getNoOfSlots()));
		} else {
			criterion.add(Restrictions.ne("freeSlot", 0));
		}

		if (queryCarpoolSlot.getPrice() != 0) {
			criterion
					.add(Restrictions.le("price", queryCarpoolSlot.getPrice()));
		}

		if (queryCarpoolSlot.getDepartureTime() != null) {
			Date lowerLimit = DateUtils.addMinutes(
					queryCarpoolSlot.getDepartureTime(),
					-queryCarpoolSlot.getTimeDeviation());
			Date upperLimit = DateUtils.addMinutes(
					queryCarpoolSlot.getDepartureTime(),
					queryCarpoolSlot.getTimeDeviation());

			criterion.add(Restrictions.and(
					Restrictions.ge("offerTime", lowerLimit),
					Restrictions.le("offerTime", upperLimit)));
		}

		return criterion.list();

	}

	public List<CarPoolSlot> getCarPoolSlot(QueryCarPool queryCarpoolSlot,
			int destinationId) {
		Criteria criterion = sessionFactory
				.getCurrentSession()
				.createCriteria(CarPoolSlot.class)
				.add(Restrictions.and(
						Restrictions.eq("destination.destinationId", destinationId),
						Restrictions.ne("userDetail.userId", queryCarpoolSlot.getUserId())));

		if (queryCarpoolSlot.getNoOfSlots() != 0) {
			criterion.add(Restrictions.ge("freeSlot",
					queryCarpoolSlot.getNoOfSlots()));
		} else {
			criterion.add(Restrictions.ne("freeSlot", 0));
		}

		if (queryCarpoolSlot.getPrice() != 0) {
			criterion
					.add(Restrictions.le("price", queryCarpoolSlot.getPrice()));
		}

		if (queryCarpoolSlot.getDepartureTime() != null) {
			Date lowerLimit = DateUtils.addMinutes(
					queryCarpoolSlot.getDepartureTime(),
					-queryCarpoolSlot.getTimeDeviation());
			Date upperLimit = DateUtils.addMinutes(
					queryCarpoolSlot.getDepartureTime(),
					queryCarpoolSlot.getTimeDeviation());
			criterion.add(Restrictions.and(
					Restrictions.ge("offerTime", lowerLimit),
					Restrictions.le("offerTime", upperLimit)));
		}

		return criterion.list();

	}

	public List<CarPoolSlot> getCarpoolSlotUsingProviderId(String userId) {

		return sessionFactory.getCurrentSession()
				.createCriteria(CarPoolSlot.class)
				.add(Restrictions.eq("userDetail.userId", userId))
				.list();

	}

	public int bookCarPoolBySlotId(BookCarPool carPool) {

		Criteria criteria = sessionFactory
				.getCurrentSession()
				.createCriteria(CarPoolBooking.class)
				.add(Restrictions.and(Restrictions.eq("id.slotId",
						carPool.getCarPoolSlotId()), Restrictions.eq(
						"id.seekerId", carPool.getUserId())));

		if (criteria.list().size() != 0) {
			return -1;
		}

		Query query = sessionFactory.getCurrentSession()
				.getNamedQuery(CarPoolSlot.BOOK_CAR_POOL_SLOT_BY_SLOT_ID)
				.setParameter("slotID", carPool.getCarPoolSlotId())
				.setParameter("noOfSlot", carPool.getNoOfSlots());

		int result = query.executeUpdate();

		bookCarPool(carPool);
		return result;

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void bookCarPool(BookCarPool carPool) {
		CarPoolBooking carPoolBooking = new CarPoolBooking();

		CarPoolBookingId bookingId = new CarPoolBookingId();

		bookingId.setSeekerId(carPool.getUserId());
		bookingId.setSlotId(carPool.getCarPoolSlotId());
		carPoolBooking.setId(bookingId);
		carPoolBooking.setLastUpdateTime(new Date());
		sessionFactory.getCurrentSession().save(carPoolBooking);

	}

	void addRestrictionIfNotNull(String columnName, String columnValue,
			Criteria criteria) {
		if (columnValue != null) {
			criteria.add(Restrictions.eq(columnName, columnValue));
		}
	}
}
