package com.carpool.chatbot;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OfferCarPool {

	@JsonProperty("user_id")
	String providerId;

	@JsonProperty("number_of_slots")
	Integer totalSlots;

	@JsonProperty("destination_name")
	String destnationName;

	@JsonProperty("departure_time")
	Date offerTime;

	@JsonProperty("departure_time_original")
	String offerTimeOriginal;

	@JsonProperty("price")
	Integer price;

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public Integer getTotalSlots() {
		return totalSlots;
	}

	public void setTotalSlots(Integer totalSlots) {
		this.totalSlots = totalSlots;
	}

	public String getDestnationName() {
		return destnationName;
	}

	public void setDestnationName(String destnationName) {
		this.destnationName = destnationName;
	}

	public Date getOfferTime() {
		return offerTime;
	}

	public void setOfferTime(Date offerTime) {
		this.offerTime = offerTime;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = Integer.parseInt(price);
	}

	public String getOfferTimeOriginal() {
		return offerTimeOriginal;
	}

	public void setOfferTimeOriginal(String offerTimeOriginal) {
		this.offerTimeOriginal = offerTimeOriginal;
	}
}
