package com.carpool.chatbot;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookCarPoolForSelectedOption {

	
	@JsonProperty("user_id")
	String userId;
	
	@JsonProperty("time")
	Date time;

	@JsonProperty("selection")
	String selection;

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	@JsonProperty("destination_name")
	String destination;


	@JsonProperty("number_of_slots")
	Integer noOfSlots;
	
	@JsonProperty("NameList")
	String NameList;
	
	public Integer getNoOfSlots() {
		return noOfSlots;
	}

	public void setNoOfSlots(Integer noOfSlots) {
		this.noOfSlots = noOfSlots;
	}

	public String getNameList() {
		return NameList;
	}

	public void setNameList(String nameList) {
		NameList = nameList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
