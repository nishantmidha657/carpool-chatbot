package com.carpool.chatbot;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookCarpoolByName {

	@JsonProperty("user_id")
	String userId;

	@JsonProperty("name")
	String name;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
