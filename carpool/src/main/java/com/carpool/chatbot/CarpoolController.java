package com.carpool.chatbot;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CarpoolController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CarpoolController.class);

	@Autowired
	private CarpoolDataServiceImpl carPoolServiceImpl;
	
	@RequestMapping(name = "/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String handleWebhook(@RequestBody String webhookRequest, HttpServletResponse response1) throws JSONException, JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received webhook request as {}", webhookRequest);
		
		JSONObject jsonObject = new JSONObject(webhookRequest);
		String intent = jsonObject.getJSONObject("queryResult").
				getJSONObject("intent").getString("displayName");
		
		String sessionUrl = jsonObject.getString("session");
		
		response1.setContentType("application/json");
		
		ObjectMapper mapper = new ObjectMapper();
		String paramJson = jsonObject.getJSONObject("queryResult").getJSONObject("parameters").toString();
		
		LOGGER.info("Processing Request for intent {} having parameters {}", intent, paramJson);
		
		String defaultResponse = "Unknown Operation. Please contact Administrator or try again";
		ResponseInfo operationResponse = null;
		
		switch (intent) {

			case "Welcome":
				String userId = new JSONObject(paramJson).getString("user_id");
				operationResponse = carPoolServiceImpl.identifyUser(userId, false);
				break;
	
			case "CollectUserId":
				String enteredId = new JSONObject(paramJson).getString("user_id");
				operationResponse = carPoolServiceImpl.identifyUser(enteredId, true);
				break;
				
			case "OfferCarPool":
				OfferCarPool offerCarPool = mapper.readValue(paramJson, OfferCarPool.class);
				operationResponse = carPoolServiceImpl.saveCarpoolOffer(offerCarPool);
				break;
	
			case "QueryCarPool":
				QueryCarPool queryCarPool = mapper.readValue(paramJson, QueryCarPool.class);
				operationResponse = carPoolServiceImpl.queryCarPoolSlot(queryCarPool);
				break;
	
			case "BookCarPool":
				BookCarPool bookCarPool = mapper.readValue(paramJson, BookCarPool.class);
				operationResponse = carPoolServiceImpl.bookCarPool(bookCarPool);
				break;
	
			case "BookAfterQuery":
				BookCarPool bookCarPoolAfterQuery = mapper.readValue(paramJson, BookCarPool.class);
				operationResponse = carPoolServiceImpl.bookCarPool(bookCarPoolAfterQuery);
				break;
			
			case "MultiDestinationIntent" :
				BookCarPoolForSelectedOption bookCarPoolForSelectedOption = mapper.readValue(paramJson, BookCarPoolForSelectedOption.class);
				operationResponse = carPoolServiceImpl.bookCarPoolForSelectedOption(bookCarPoolForSelectedOption);
                break;
			case "BookBYName" :
				BookCarpoolByName bookCarPoolByName= mapper.readValue(paramJson, BookCarpoolByName.class);
				operationResponse = carPoolServiceImpl.bookCarpoolByName(bookCarPoolByName);
				break;
			case "BookBYName-yes":
			    BookBYNameYes bookBYNameYes=mapper.readValue(paramJson,BookBYNameYes.class);
			    operationResponse =  carPoolServiceImpl.bookCarPool(bookBYNameYes);
			                       
			default:
				break;
		}

		if(operationResponse == null) {
			operationResponse = new ResponseInfo(defaultResponse);
		}
		
		return constructResponseJson(sessionUrl, operationResponse);
	}
	
	private String constructResponseJson(String sessionUrl, ResponseInfo responseInfo) {
		JSONObject responseJson = new JSONObject();
		
		responseJson.put("fulfillmentText", responseInfo.getFulfillmentText());
		
		if(responseInfo.getContext() != null) {
			ResponseInfo.OutputContext context = responseInfo.getContext();
			
			JSONObject contextJson = new JSONObject().put("name", sessionUrl + "/contexts/" + context.name);
			contextJson.put("lifespanCount", "5");
			JSONObject contextParamJson = new JSONObject();
			for(Entry<String, String> param: context.params.entrySet()) {
				contextParamJson.put(param.getKey(), param.getValue());
			}
			contextJson.put("parameters", contextParamJson);
			
			JSONArray contextArray = new JSONArray("[ " + contextJson.toString() + " ]");
			responseJson.put("outputContexts", contextArray);
		}
		
		if(responseInfo.getQuickReply() != null) {
			ResponseInfo.SkypeQuickReply quickReply = responseInfo.getQuickReply();
			
			JSONObject quickReplyJson = new JSONObject().put("title", quickReply.title);
			JSONArray optionsArray = new JSONArray(quickReply.options);
			quickReplyJson.put("quickReplies", optionsArray);
			
			JSONObject fulfillmentMessage = new JSONObject();
			fulfillmentMessage.put("platform", "SKYPE");
			fulfillmentMessage.put("quickReplies", quickReplyJson);
			
			// When fulfillmentMessages element is included in response, the Default Response
			// should also be included in it
			JSONObject defaultTextResponse = new JSONObject();
			JSONArray textArray = new JSONArray();
			textArray.put(responseInfo.getFulfillmentText());
			defaultTextResponse.put("text", (new JSONObject()).put("text", textArray));
			
			JSONArray fulfillmentMessageArray = new JSONArray();
			fulfillmentMessageArray.put(fulfillmentMessage).put(defaultTextResponse);
			responseJson.put("fulfillmentMessages", fulfillmentMessageArray);
		}
		
		LOGGER.info("Response sent as " + responseJson.toString() );
		
		return responseJson.toString();
	}

	@ExceptionHandler
	private void handleException(Exception ex) {
		LOGGER.warn("Received exception : {}", ex.toString());
		LOGGER.debug(ExceptionUtils.getFullStackTrace(ex));
	}
}
