package com.carpool.chatbot;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryCarPool {

	@Override
	public String toString() {
		return "QueryCarPool [destination=" + destination + ", departureTime="
				+ departureTime + ", departureTimeOriginal="
				+ departureTimeOriginal + ", price=" + price + ", noOfSlots="
				+ noOfSlots + ", userId=" + userId + ", timeDeviation="
				+ timeDeviation + "]";
	}

	@JsonProperty("destination_name")
	String destination;
	
	@JsonProperty("departure_time")
	Date departureTime;
	
	@JsonProperty("departure_time_original")
	String departureTimeOriginal;
	
	@JsonProperty("price")
	Integer price;
	
	@JsonProperty("number_of_slots")
	Integer noOfSlots;
	
	@JsonProperty("user_id")
	String userId;
	
	@JsonProperty("time_deviation_minutes")
	Integer timeDeviation;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = Integer.parseInt(price);
	}

	public Integer getNoOfSlots() {
		return noOfSlots;
	}

	public void setNoOfSlots(Integer noOfSlots) {
		this.noOfSlots = noOfSlots;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDepartureTimeOriginal() {
		return departureTimeOriginal;
	}

	public void setDepartureTimeOriginal(String departureTimeOriginal) {
		this.departureTimeOriginal = departureTimeOriginal;
	}

	public Integer getTimeDeviation() {
		return timeDeviation;
	}

	public void setTimeDeviation(Integer timeDeviation) {
		this.timeDeviation = timeDeviation;
	}
}
