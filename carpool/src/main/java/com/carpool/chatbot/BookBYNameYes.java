package com.carpool.chatbot;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookBYNameYes {

	@JsonProperty("user_id")
	String userId;

	@JsonProperty("carpool_slot_id")
	Integer carPoolSlotId;
	
	public Integer getNoOfSlots() {
		return noOfSlots;
	}

	public void setNoOfSlots(Integer noOfSlots) {
		this.noOfSlots = noOfSlots;
	}

	@JsonProperty("number_of_slots")
	Integer noOfSlots;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getCarPoolSlotId() {
		return carPoolSlotId;
	}

	public void setCarPoolSlotId(Integer carPoolSlotId) {
		this.carPoolSlotId = carPoolSlotId;
	}

}
