package com.carpool.chatbot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carpool.chatbot.dao.CarPoolDaoImpl;
import com.carpool.chatbot.entity.CarPoolSlot;
import com.carpool.chatbot.entity.Destination;
import com.carpool.chatbot.entity.UserDetail;

@Component
public class CarpoolDataServiceImpl {

	public static final String UNKNOWN_USER_ERROR = "Unable to recognize you. Please provide your Id";
	public static final String INVALID_USER_ERROR = "User Id %s is not valid. Please provide a alternate one";
	public static final String USER_GREETING_MESSAGE = "Greetings %s! Welcome to Carpool Booking bot. How can I help you today?";
	public static final String INVALID_DESTINATION_ERROR_MESSAGE = "Destination %s is not served. Please contact administrator";
	public static final String NO_RIDE_ERROR_MESSAGE = "No ride available at the moment for %s around %s";
	public static final String RIDE_AVAILABLE_MESSAGE = "Carpool available by %s for %s at %s";
	public static final String CARPOOL_OFFER_MESSAGE = "Carpool offer details stored successfully for %s having departure at %s";

	public static final String BOOKING_CONFLICT_MESSAGE = "Conflict detected while booking the carpool, Please try again!";
	public static final String SLOT_ID_BOOKING_SUCCESS_MESSAGE = "Your Carpool to %s has been successfully booked with %s at %s.";
	public static final String CARPOOL_ALREADY_BOOKED = "Your booking is already present for this carpool by %s to %s";
	public static final String CARPOOL_ALREADY_AVAILABLE = "Your carpool offer is already available for %s with departure time as %s";

	public static final String MULTIPLE_CARPOOL_AVAILABLE = "%s have carpool available to %s around %s. With whom you would like to travel?";
	public static final String MULTIPLE_CARPOOL_AVAILABLE_SKYPE_TITLE = "Many carpools available to %s around %s. Select one.";
	public static final String NO_OFFER_AVAILABLE_FROM_USER = "Sorry, There is no carpool offered by %s";
	
	public static final String USER_NOT_KNOWN_MESSAGE = "Sorry, I don't know %s, Say my hello to him";
	
	public static final String NO_OFFER_FROM_USER = "Oh! %s has not offered any carpool today";
		
	public static final String RIDE_AVAIALABLE_FROM_USER = "%s will start for %s at %s, Should I confirm the carpool?";
	
	@Autowired
	CarPoolDaoImpl carPoolDaoImpl;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
	
	public ResponseInfo identifyUser(String userId, boolean userEnteredId) {

		// All user id are lowercase in DB; change to lowerca
		UserDetail user = carPoolDaoImpl.getUserDetail(userId.toLowerCase());
		
		ResponseInfo response;
		
		if(user == null) {
			// If user provided the Id, prompt with a different message
			
			// An proper userid would only have number + characters (max 20) but no spaces
			boolean idProper = userId.length() <= 20 && !userId.contains(" ");
			
			if(userEnteredId && idProper) {
				response = new ResponseInfo(String.format(INVALID_USER_ERROR, userId));
			} else {			
				ResponseInfo.OutputContext userNotValidatedContext = new ResponseInfo.OutputContext();
				userNotValidatedContext.name = "user_not_validated";
				response = new ResponseInfo(UNKNOWN_USER_ERROR);
				response.setContext(userNotValidatedContext);
			}
			
			return response;
		}
		
		response = new ResponseInfo(String.format(USER_GREETING_MESSAGE,user.getFirstName()));
		ResponseInfo.OutputContext userValidatedContext = new ResponseInfo.OutputContext();
		userValidatedContext.name = "user_validated";
		userValidatedContext.params.put("user_id", user.getUserId());
		response.setContext(userValidatedContext);
		return response;
	}

	public ResponseInfo saveCarpoolOffer(OfferCarPool offerCarPool) {

		CarPoolSlot carPoolSlot = new CarPoolSlot();

		List<Destination> destinationId = carPoolDaoImpl
				.findDestination(offerCarPool.getDestnationName());

		if (destinationId.size() == 0) {
			return new ResponseInfo(String.format(
					INVALID_DESTINATION_ERROR_MESSAGE,
					offerCarPool.getDestnationName()));
		}

		UserDetail userDetail = new UserDetail(offerCarPool.getProviderId());

		carPoolSlot.setUserDetail(userDetail);
		carPoolSlot.setDestination(destinationId.get(0));

		carPoolSlot.setFreeSlot(offerCarPool.getTotalSlots());
		carPoolSlot.setTotalSlot(offerCarPool.getTotalSlots());

		carPoolSlot.setLastUpdateTime(new Date());
		carPoolSlot.setOfferTime(offerCarPool.getOfferTime());
		carPoolSlot.setPrice(offerCarPool.getPrice());

		String offerTime = carPoolDaoImpl.checkCarPoolAlreadyAvailable(destinationId
				.get(0).getDestinationId(), userDetail.getUserId());

		if (offerTime != null) {
			return new ResponseInfo(String.format(CARPOOL_ALREADY_AVAILABLE,
					offerCarPool.getDestnationName(), offerTime));
		}

		carPoolDaoImpl.saveCarPoolOffer(carPoolSlot);

		ResponseInfo response = new ResponseInfo(String.format(CARPOOL_OFFER_MESSAGE, offerCarPool.getDestnationName(),offerCarPool.getOfferTimeOriginal()));
		ResponseInfo.OutputContext offerSavedContext = new ResponseInfo.OutputContext();
		offerSavedContext.name = "offer_carpool_success";
		response.setContext(offerSavedContext);
		return response;
	}

	public ResponseInfo queryCarPoolSlot(QueryCarPool queryCarpoolSlot) {

		// Remove any ? symbol from destination time
		queryCarpoolSlot.setDepartureTimeOriginal(queryCarpoolSlot
				.getDepartureTimeOriginal().replaceAll("\\?", ""));

		// Retrieve destination id using destination name
		List<Destination> destinationList = carPoolDaoImpl
				.findDestination(queryCarpoolSlot.getDestination());

		if (destinationList.size() == 0) {
			return new ResponseInfo(String.format(
					INVALID_DESTINATION_ERROR_MESSAGE,
					queryCarpoolSlot.getDestination()));
		}

		int destinationId = destinationList.get(0).getDestinationId();

		List<CarPoolSlot> carPoolSlots = carPoolDaoImpl.getCarPoolSlot(
				queryCarpoolSlot, destinationId);

		if (carPoolSlots.size() == 0) {
			return new ResponseInfo(String.format(NO_RIDE_ERROR_MESSAGE,
					queryCarpoolSlot.getDestination(),
					queryCarpoolSlot.getDepartureTimeOriginal()));

		}

		if (carPoolSlots.size() > 1) {

			ResponseInfo.OutputContext querySuccessContext = new ResponseInfo.OutputContext();
			querySuccessContext.name = "query_carpool_list";

			List<String> nameListStandard = new ArrayList<String>();
			List<String> nameListSkype = new ArrayList<String>();
			int count = 0;
			JSONObject  jsonObject=new JSONObject();
			for (CarPoolSlot carPoolSlot : carPoolSlots) {
				count++;
				if (count > 3) {
					break;
				}
				
				
				jsonObject.put(carPoolSlot.getUserDetail().getFirstName(), carPoolSlot.getSlotId());
				nameListStandard.add(carPoolSlot.getUserDetail().getFirstName() + " [" +
				dateFormat.format(carPoolSlot.getOfferTime()) + "]" );
				
				nameListSkype.add(carPoolSlot.getUserDetail().getFirstName() + " at " +
				dateFormat.format(carPoolSlot.getOfferTime()));
				
				querySuccessContext.params.put("nameList", jsonObject.toString(1));
			}

			// Construct a list of users offering the pool
			int index = 0;
			StringBuilder offererNames = new StringBuilder();
			while(true) {
				offererNames.append(nameListStandard.get(index));
				index++;
				if(index < (nameListStandard.size() - 1)) {
					offererNames.append(", ");
				} else {
					offererNames.append(" and ");
					offererNames.append(nameListStandard.get(index));
					break;
				}
			}
			
			ResponseInfo responseInfo = new ResponseInfo(String.format(
					MULTIPLE_CARPOOL_AVAILABLE,
					offererNames, destinationList
							.get(0).getDestinationName(), queryCarpoolSlot.getDepartureTimeOriginal()));
			responseInfo.setContext(querySuccessContext);
			
			// Also add multiple offerer names in skype quick reply format
			ResponseInfo.SkypeQuickReply quickReply = new ResponseInfo.SkypeQuickReply();
			quickReply.title = String.format(MULTIPLE_CARPOOL_AVAILABLE_SKYPE_TITLE, destinationList
							.get(0).getDestinationName(), queryCarpoolSlot.getDepartureTimeOriginal());
			quickReply.options = nameListSkype;
			responseInfo.setQuickReply(quickReply);
			
			return responseInfo;
		}
		

		// Pick the first available slot, explore context then give options for
		// multiple options
		CarPoolSlot carPoolSlot = carPoolSlots.get(0);

		ResponseInfo response = new ResponseInfo(String.format(
				RIDE_AVAILABLE_MESSAGE, carPoolSlot
						.getUserDetail().getFirstName(), carPoolSlot
						.getDestination().getDestinationName(),
				dateFormat.format(carPoolSlot.getOfferTime())));
		ResponseInfo.OutputContext querySuccessContext = new ResponseInfo.OutputContext();
		querySuccessContext.name = "query_carpool_success";

		querySuccessContext.params.put("carpool_slot_id",
				String.valueOf(carPoolSlot.getSlotId()));
		response.setContext(querySuccessContext);
		return response;
	}

	public ResponseInfo bookCarPoolForSelectedOption(BookCarPoolForSelectedOption carPoolForSelectedOption){
	JSONObject  jsonObject=new JSONObject(carPoolForSelectedOption.getNameList());
		BookCarPool bookCarPool=new BookCarPool();

		try {
			jsonObject.get(carPoolForSelectedOption.getSelection());
		} catch (JSONException exception) {

			return new ResponseInfo(String.format(NO_OFFER_AVAILABLE_FROM_USER,
					carPoolForSelectedOption.getSelection()));
		}

	bookCarPool.setCarPoolSlotId(jsonObject.getInt(carPoolForSelectedOption.getSelection()));
	bookCarPool.setNoOfSlots(carPoolForSelectedOption.getNoOfSlots());
	bookCarPool.setUserId(carPoolForSelectedOption.getUserId());
	bookCarPool.setDestination(carPoolForSelectedOption.getDestination());
	
	CarPoolSlot slot = carPoolDaoImpl.getCarPoolSlot(bookCarPool.getCarPoolSlotId());
	int isSlotBooked = carPoolDaoImpl.bookCarPoolBySlotId(bookCarPool);

	if (isSlotBooked == 0) {
		return new ResponseInfo(BOOKING_CONFLICT_MESSAGE);
	} else if (isSlotBooked == -1) {
		return new ResponseInfo(String.format(CARPOOL_ALREADY_BOOKED,
				slot.getUserDetail().getFirstName(),
				bookCarPool.getDestination()));
	} else {
		
		return new ResponseInfo(String.format(
				SLOT_ID_BOOKING_SUCCESS_MESSAGE,
				slot.getDestination().getDestinationName(),
				slot.getUserDetail().getFirstName(),
				dateFormat.format(slot.getOfferTime())));
	}

	
	}
	
	public ResponseInfo bookCarpoolByName(BookCarpoolByName bookCarPoolByName) {

		//Find any user by name 
		
		List<UserDetail> userDetails=carPoolDaoImpl.getUserDetailByFirstName(bookCarPoolByName.getName());
		
		if(userDetails.size() == 0){
			return new ResponseInfo(String.format(USER_NOT_KNOWN_MESSAGE, bookCarPoolByName.getName()));
		}
		
		UserDetail firstUser=userDetails.get(0);
		
		List<CarPoolSlot> carPoolSlot=carPoolDaoImpl.getCarpoolSlotUsingProviderId(firstUser.getUserId());

		if (carPoolSlot.size() == 0) {
			return new ResponseInfo(String.format(NO_OFFER_FROM_USER,
					bookCarPoolByName.getName()));
		}
		
		if (carPoolSlot.get(0).getFreeSlot() == 0) {
			return new ResponseInfo(String.format(
					"Bad Luck! No vaccant seat available with %s",
					bookCarPoolByName.getName()));
		}

		ResponseInfo responseInfo= new ResponseInfo(String.format(RIDE_AVAIALABLE_FROM_USER, firstUser.getFirstName(),carPoolSlot.get(0).getDestination().getDestinationName(),dateFormat.format(carPoolSlot.get(0).getOfferTime())))
		;
		
		ResponseInfo.OutputContext querySuccessContext = new ResponseInfo.OutputContext();
		querySuccessContext.name = "query_by_name_successfull";

		querySuccessContext.params.put("carpool_slot_id",
				String.valueOf(carPoolSlot.get(0).getSlotId()));
		responseInfo.setContext(querySuccessContext);
		
		return responseInfo;
	}	

	public ResponseInfo bookCarPool(BookBYNameYes bookBYNameYes) {
		BookCarPool bookCarPool = new BookCarPool();
		bookCarPool.setUserId(bookBYNameYes.getUserId());
		bookCarPool.setCarPoolSlotId(bookBYNameYes.getCarPoolSlotId());
		bookCarPool.setNoOfSlots(bookBYNameYes.getNoOfSlots());
		return bookCarpoolBySlotId(bookCarPool);
	}

	public ResponseInfo bookCarPool(BookCarPool bookCarPool) {
		// Retrieve destination id using destination name
		List<Destination> destinationList = carPoolDaoImpl
				.findDestination(bookCarPool.getDestination());

		if (destinationList.size() == 0) {
			return new ResponseInfo(String.format(
					INVALID_DESTINATION_ERROR_MESSAGE,
					bookCarPool.getDestination()));
		}

		int destinationId = destinationList.get(0).getDestinationId();
	
		ResponseInfo responseInfo = bookCarpoolBySlotId(bookCarPool);
		
		if (responseInfo != null ){
			return responseInfo;
		}
		
		List<CarPoolSlot> carPoolSlots = carPoolDaoImpl.getCarPoolSlot(
				bookCarPool, destinationId);

		if (carPoolSlots.size() == 0) {
			return new ResponseInfo((String.format(NO_RIDE_ERROR_MESSAGE,
					bookCarPool.getDestination(),
					bookCarPool.getDepartureTimeOriginal())));
		}

		// Pick the first available slot, explore context then give options for
		// multiple options
		CarPoolSlot carPoolSlot = carPoolSlots.get(0);

		bookCarPool.setCarPoolSlotId(carPoolSlot.getSlotId());
		
		CarPoolSlot slot = carPoolDaoImpl.getCarPoolSlot(bookCarPool.getCarPoolSlotId());
		
		int isSlotBooked = carPoolDaoImpl.bookCarPoolBySlotId(bookCarPool);

		if (isSlotBooked == 0) {
			return new ResponseInfo(BOOKING_CONFLICT_MESSAGE);
		} else if (isSlotBooked == -1) {

			return new ResponseInfo(String.format(CARPOOL_ALREADY_BOOKED,
					slot.getUserDetail().getFirstName(),
					bookCarPool.getDestination()));
		} else {
			
			return new ResponseInfo(String.format(
					SLOT_ID_BOOKING_SUCCESS_MESSAGE,
					slot.getDestination().getDestinationName(),
					slot.getUserDetail().getFirstName(),
					dateFormat.format(slot.getOfferTime())));
		}
	}
	
	public ResponseInfo bookCarpoolBySlotId(BookCarPool bookCarPool){
		// Code to handle booking afetr a query
		if (bookCarPool.getCarPoolSlotId() != 0) {
			int isSlotBooked = carPoolDaoImpl.bookCarPoolBySlotId(bookCarPool);

			CarPoolSlot slot = carPoolDaoImpl.getCarPoolSlot(bookCarPool.getCarPoolSlotId());
			
			if (isSlotBooked == 0) {
				return new ResponseInfo(BOOKING_CONFLICT_MESSAGE);
			} else if (isSlotBooked == -1) {
				String text= String.format(CARPOOL_ALREADY_BOOKED,
						slot.getUserDetail().getFirstName(),
						slot.getDestination());
				
				return new ResponseInfo(text);
			} else {
				return new ResponseInfo(String.format(
						SLOT_ID_BOOKING_SUCCESS_MESSAGE,
						slot.getDestination().getDestinationName(),
						slot.getUserDetail().getFirstName(),
						dateFormat.format(slot.getOfferTime())));
				}
		}

		return null;
	}
}
