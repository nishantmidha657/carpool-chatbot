package com.carpool.chatbot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseInfo {

	public static class OutputContext {
		public String name;
		public Map<String, String> params = new HashMap<>();
	}
	
	public static class SkypeQuickReply {
		public String title;
		public List<String> options;
	}
	
	public ResponseInfo(String responseText) {
		this.fulfillmentText = responseText;
		this.context = null;
		this.quickReply = null;
	}

	private String fulfillmentText;
	private OutputContext context;
	private SkypeQuickReply quickReply;
	
	public String getFulfillmentText() {
		return fulfillmentText;
	}

	public void setFulfillmentText(String fulfillmentText) {
		this.fulfillmentText = fulfillmentText;
	}

	public OutputContext getContext() {
		return context;
	}

	public void setContext(OutputContext context) {
		this.context = context;
	}

	public SkypeQuickReply getQuickReply() {
		return quickReply;
	}

	public void setQuickReply(SkypeQuickReply quickReply) {
		this.quickReply = quickReply;
	}
}
