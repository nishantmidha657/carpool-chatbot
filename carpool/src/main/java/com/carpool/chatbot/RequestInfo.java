package com.carpool.chatbot;

import java.util.HashMap;
import java.util.Map;

public class RequestInfo {

	private String intent;
	private String queryText;
	private boolean allParamsPresent;
	
	private Map<String, String> params = new HashMap<String, String>();

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	public String getQueryText() {
		return queryText;
	}

	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	public boolean isAllParamsPresent() {
		return allParamsPresent;
	}

	public void setAllParamsPresent(boolean allParamsPresent) {
		this.allParamsPresent = allParamsPresent;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return "RequestInfo [intent=" + intent + ", queryText=" + queryText + ", allParamsPresent=" + allParamsPresent
				+ ", params=" + params + "]";
	}
}
